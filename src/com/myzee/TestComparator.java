package com.myzee;

import java.util.Arrays;
import java.util.Comparator;
import java.util.TreeSet;

public class TestComparator {
		public static void main(String[] args) {
			// TODO Auto-generated method stub
			Employee e1 = new Employee(10, "nag");
			Employee e2 = new Employee(38, "pat");
			Employee e3 = new Employee(176, "bee");
			Employee e4 = new Employee(144, "fly");
			Employee e5 = new Employee(3, "xyz");
			
			TreeSet<Employee> t = new TreeSet<>(new Employee());
			t.add(e1);
			t.add(e2);
			t.add(e3);
			t.add(e4);
			t.add(e5);
			
			System.out.println(t);
			
			System.out.println("\nSorting employees based on names\n");
			Employee[] arr = new Employee[5];
			arr[0] = e1;
			arr[1] = e2;
			arr[2] = e3;
			arr[3] = e4;
			arr[4] = e5;
			
			Comparator<Employee> nameComparator = new Comparator<Employee>() {

				@Override
				public int compare(Employee o1, Employee o2) {
					// TODO Auto-generated method stub
//					return ((Integer)o1.getEid()).compareTo((Integer)o2.getEid());
					return (o1.getEname()).compareTo(o2.getEname());
				}
				
			};
			
			Arrays.sort(arr, nameComparator);
			for(Employee e : arr) {
				System.out.println(e);
			}
			
	}

}
