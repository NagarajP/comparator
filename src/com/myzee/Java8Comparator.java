package com.myzee;

import java.util.Arrays;
import java.util.Comparator;

public class Java8Comparator {

	public static void main(String[] args) {
		
		Employee e1 = new Employee(10, "nag");
		Employee e2 = new Employee(38, "pat");
		Employee e3 = new Employee(176, "bee");
		Employee e4 = new Employee(144, "fly");
		Employee e5 = new Employee(3, "xyz");
		
		Employee[] arr = new Employee[5];
		arr[0] = e1;
		arr[1] = e2;
		arr[2] = e3;
		arr[3] = e4;
		arr[4] = e5;
		
		//or Using java8 lambda expression
		System.out.println("\nUsing lambda expression");
		
		Comparator<Employee> idComparator = (emp1, emp2) -> {
			return Integer.compare(emp1.getEid(), emp2.getEid());
		};
		
		Arrays.sort(arr, idComparator);
		Arrays.stream(arr).forEach(e -> System.out.println(e));
	}

}
