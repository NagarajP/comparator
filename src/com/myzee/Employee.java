package com.myzee;

import java.util.Comparator;

public class Employee implements Comparator<Employee>{

	private int eid;
	private String ename;
	
	public Employee() {
		// TODO Auto-generated constructor stub
	}
	
	public Employee(int eid, String ename) {
		super();
		this.eid = eid;
		this.ename = ename;
	}



	/**
	 * @return the eid
	 */
	public int getEid() {
		return eid;
	}

	/**
	 * @param eid the eid to set
	 */
	public void setEid(int eid) {
		this.eid = eid;
	}

	/**
	 * @return the ename
	 */
	public String getEname() {
		return ename;
	}

	/**
	 * @param ename the ename to set
	 */
	public void setEname(String ename) {
		this.ename = ename;
	}

	@Override
	public int compare(Employee o1, Employee o2) {
		// TODO Auto-generated method stub
//		System.out.println(o1.ename);
//		System.out.println(o2.ename);
		return (o1.ename).compareTo((o2.ename));
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "[eid=" + eid + ", ename=" + ename + "]";
	}
}
